﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebHelper.cs" company="MightCall.Ru">
//   Test task
// </copyright>
// <summary>
//   Defines the WebHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Helpers
{
	using System;
	using System.Linq;

	/// <summary>
	/// The web helper.
	/// </summary>
	public class WebHelper
	{
		/// <summary>
		/// The max input data length.
		/// </summary>
		public readonly int MaxInputDataLength = 60;

		/// <summary>
		/// Gets or sets the login.
		/// </summary>
		public string Login { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// Gets or sets the host.
		/// </summary>
		public string Host { get; set; }

		/// <summary>
		/// Gets or sets the url.
		/// </summary>
		public string Url { get; set; }

		/// <summary>
		/// The get random string.
		/// </summary>
		/// <param name="symbolCount">
		/// The symbol count.
		/// </param>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public string GetRandomString(int symbolCount)
		{
			Random rnd = new Random();
			var otp = string.Concat(Enumerable.Range(0, symbolCount)
				.Select(i => (char)(rnd.Next(26) + 'A')));
			return otp;
		}

		/// <summary>
		/// The get valid user.
		/// </summary>
		/// <returns>
		/// The <see cref="WebHelper"/>.
		/// </returns>
		public WebHelper GetValidUser()
		{
			return new WebHelper
			{
					       Url = "https://telephony.yandex.ru/",
					       Login = "alexander.pokidin@yandex.ru",
					       Password = "mp7ftLJA77uf",
				       };
		}
	}
}

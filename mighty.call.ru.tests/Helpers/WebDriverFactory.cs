﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebDriverFactory.cs" company="MightyCall.ru">
//   Test task
// </copyright>
// <summary>
//   Defines the WebDriverFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Helpers
{
	using System.Collections;

	using NUnit.Framework;

	/// <summary>
	/// The web driver factory.
	/// </summary>
	public class WebDriverFactory
	{
		/// <summary>
		/// The drivers.
		/// </summary>
		/// <returns>
		/// The <see cref="IEnumerable"/>.
		/// </returns>
		public static IEnumerable Drivers()
		{
			yield return new TestCaseData(WebDriversHelper.GetChromeDriver()).SetName("Chrome");
			yield return new TestCaseData(WebDriversHelper.GetFirefoxDriver()).SetName("Firefox");
		}
	}
}

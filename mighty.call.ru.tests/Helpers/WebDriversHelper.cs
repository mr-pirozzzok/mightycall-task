﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebDriversHelper.cs" company="MightyCall.ru">
//   Test task
// </copyright>
// <summary>
//   Defines the WebDriversHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Helpers
{
	using OpenQA.Selenium.Chrome;
	using OpenQA.Selenium.Firefox;
	using OpenQA.Selenium.IE;

	/// <summary>
	/// The web drivers helper.
	/// </summary>
	public static class WebDriversHelper
	{
		/// <summary>
		/// The get chrome driver.
		/// </summary>
		/// <returns>
		/// The <see cref="ChromeDriver"/>.
		/// </returns>
		public static ChromeDriver GetChromeDriver()
		{
			return new ChromeDriver();
		}

		/// <summary>
		/// The get firefox driver.
		/// </summary>
		/// <returns>
		/// The <see cref="FirefoxDriver"/>.
		/// </returns>
		public static FirefoxDriver GetFirefoxDriver()
		{
			return new FirefoxDriver();
		}

		/// <summary>
		/// The get internet explorer browser.
		/// </summary>
		/// <returns>
		/// The <see cref="InternetExplorerDriver"/>.
		/// </returns>
		public static InternetExplorerDriver GetInternetExplorerBrowser()
		{
			var options = new InternetExplorerOptions()
				              {
					              // InitialBrowserUrl = baseURL,
					              IntroduceInstabilityByIgnoringProtectedModeSettings = true,
					              IgnoreZoomLevel = true,
					              EnableNativeEvents = false
				              };
			return new InternetExplorerDriver(options);	
		}
	}
}

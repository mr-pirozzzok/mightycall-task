﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebDriverSourceAttribute.cs" company="MightyCall.ru">
//   Test task
// </copyright>
// <summary>
//   Defines the WebDriverSourceAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Helpers
{
	using NUnit.Framework;

	/// <summary>
	/// The web driver source attribute.
	/// </summary>
	public class WebDriverSourceAttribute : TestCaseSourceAttribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="WebDriverSourceAttribute"/> class.
		/// </summary>
		public WebDriverSourceAttribute() : base(typeof(WebDriverFactory), "Drivers")
		{
		}
	}
}

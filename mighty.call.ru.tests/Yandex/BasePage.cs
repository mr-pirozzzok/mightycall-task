﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasePage.cs" company="MightyCall.ru">
//   Test task
// </copyright>
// <summary>
//   Defines the BasePage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Yandex
{
	using OpenQA.Selenium;

	/// <summary>
	/// The base page.
	/// </summary>
	public abstract class BasePage
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BasePage"/> class.
		/// </summary>
		/// <param name="driver">
		/// The driver.
		/// </param>
		protected BasePage(IWebDriver driver) => Driver = driver;

		/// <summary>
		/// Gets or sets the url.
		/// </summary>
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets the driver.
		/// </summary>
		protected IWebDriver Driver { get; set; }
	}
}

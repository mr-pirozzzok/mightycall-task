﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProfilePage.cs" company="MightyCall.ru">
//   Test task
// </copyright>
// <summary>
//   Defines the ProfilePage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Yandex.Telephony.Profile
{
	using System;

	using Mighty.Call.Ru.Tests.Yandex;
	using Mighty.Call.Ru.Tests.Yandex.Telephony.Profile.Details;

	using OpenQA.Selenium;
	using OpenQA.Selenium.Support.UI;
	
	/// <summary>
	/// The profile page.
	/// </summary>
	public class ProfilePage : BasePage
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProfilePage"/> class.
		/// </summary>
		/// <param name="webDriver">
		/// The web driver.
		/// </param>
		public ProfilePage(IWebDriver webDriver) : base(webDriver)
		{
			PrivateSettingsBlock = new PrivateSettingsBlock(webDriver);
		}
		
		/// <summary>
		/// Gets the private settings block.
		/// </summary>
		public PrivateSettingsBlock PrivateSettingsBlock { get; private set; }

		/// <summary>
		/// Gets or sets the avatar ico.
		/// </summary>
		private IWebElement AvatarIco { get; set; }

		/// <summary>
		/// Gets or sets the profile item.
		/// </summary>
		private IWebElement ProfileItem { get; set; }

		/// <summary>
		/// The avatar ico click.
		/// </summary>
		public void AvatarIcoClick()
		{

			AvatarIco = new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[2]/div[1]/div[3]/div[2]/div[2]/div[1]/div[1]/img")));
			AvatarIco.Click();
		}

		/// <summary>
		/// The profile item click.
		/// </summary>
		public void ProfileItemClick()
		{
			ProfileItem = Driver.FindElement(By.PartialLinkText("Профиль"));
			ProfileItem.Click();
		}
	}
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PrivateSettingsBlock.cs" company="MightyCall.Ru">
//   Test task
// </copyright>
// <summary>
//   Defines the PrivateSettingsBlock type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Yandex.Telephony.Profile.Details
{
	using System;

	using OpenQA.Selenium;
	using OpenQA.Selenium.Support.PageObjects;
	using OpenQA.Selenium.Support.UI;

	/// <summary>
	/// The private settings block.
	/// </summary>
	public class PrivateSettingsBlock : BasePage
	{
		/// <inheritdoc />
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mighty.Call.Ru.Tests.Yandex.Telephony.Profile.Details.PrivateSettingsBlock" /> class.
		/// </summary>
		/// <param name="driver">
		/// The driver.
		/// </param>
		public PrivateSettingsBlock(IWebDriver driver)
			: base(driver)
		{
			PageFactory.InitElements(this.Driver, this);
		}

		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		[FindsBy(How = How.Id, Using = "FirstName")]
		public IWebElement FirstName { get; set; }

		/// <summary>
		/// Gets or sets the invalid credentials message.
		/// </summary>
		[FindsBy(How = How.CssSelector, Using = "div[class= 'popover popover-400px popover-error fade top in']")]
		public IWebElement InvalidDataMessage { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		[FindsBy(How = How.Id, Using = "LastName")]
		public IWebElement LastName { get; set; }

		/// <summary>
		/// Gets or sets the time zone.
		/// </summary>
		[FindsBy(How = How.CssSelector, Using = "span[id^=\'select2-chosen-\']")]
		public IWebElement TimeZone { get; set; }

		/// <summary>
		/// Gets or sets the cancel button.
		/// </summary>
		[FindsBy(How = How.XPath, Using = "//*[@id=\"ProfileViewModel_Forms_profileDetailsForm\"]/div[2]/div/button[1]")]
		public IWebElement CancelButton { get; set; }

		/// <summary>
		/// Gets or sets the save changes button.
		/// </summary>
		[FindsBy(How = How.XPath, Using = "//*[@id=\"ProfileViewModel_Forms_profileDetailsForm\"]/div[2]/div/button[2]")]
		public IWebElement SaveChangesButton { get; set; }

		/// <summary>
		/// Gets the success informer.
		/// </summary>
		public IWebElement SuccessInformer =>
			Driver.FindElement(
				By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Помощь'])[1]/following::span[1]"));

		/// <summary>
		/// Gets or sets the upload button.
		/// </summary>
		[FindsBy(How = How.Id, Using = "upload_btn")]
		public IWebElement UploadButton { get; set; }

		/// <summary>
		/// Gets or sets the first name caption.
		/// </summary>
		[FindsBy(How = How.XPath, Using = "//*[@id=\"ProfileViewModel_Forms_profileDetailsForm\"]/div[1]/div/div[1]/div/div[1]/label")]
		public IWebElement FirstNameCaption { get; set; }

		/// <summary>
		/// Gets or sets the last name caption.
		/// </summary>
		[FindsBy(How = How.XPath, Using = "//*[@id=\"ProfileViewModel_Forms_profileDetailsForm\"]/div[1]/div/div[1]/div/div[2]/label")]
		public IWebElement LastNameCaption { get; set; }

		/// <summary>
		/// The clear first name.
		/// </summary>
		public void ClearFirstName()
		{
			this.FirstName.Clear();
		}

		/// <summary>
		/// The clear last name.
		/// </summary>
		public void ClearLastName()
		{
			this.LastName.Clear();
		}

		/// <summary>
		/// The set first name.
		/// </summary>
		/// <param name="value">
		/// The value.
		/// </param>
		public void SetFirstName(string value)
		{
			FirstName.SendKeys(value);
		}

		/// <summary>
		/// The get first name.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public string GetFirstName()
		{
			return FirstName.GetAttribute("value");
		}

		/// <summary>
		/// The set last name.
		/// </summary>
		/// <param name="value">
		/// The value.
		/// </param>
		public void SetLastName(string value)
		{
			this.LastName.SendKeys(value);
		}

		/// <summary>
		/// The get last name.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public string GetLastName()
		{
			return this.LastName.GetAttribute("value");
		}

		/// <summary>
		/// The set time zone.
		/// </summary>
		/// <param name="value">
		/// The value.
		/// </param>
		public void SetTimeZone(string value)
		{
			TimeZone.SendKeys(value);
		}

		/// <summary>
		/// The get time zone.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public string GetTimeZone()
		{
			return TimeZone.GetAttribute("value");
		}

		/// <summary>
		/// The cancel button click.
		/// </summary>
		public void CancelButtonClick()
		{
			CancelButton.Click();
		}

		/// <summary>
		/// The save changes button click.
		/// </summary>
		public void SaveChangesButtonClick()
		{
			SaveChangesButton.Click();
		}

		/// <summary>
		/// The upload button click.
		/// </summary>
		public void UploadButtonClick()
		{
			UploadButton.Click();
		}

		/// <summary>
		/// The close success ui informer.
		/// </summary>
		public void CloseSuccessUiInformer()
		{
			new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Помощь'])[1]/following::span[1]")));
			this.SuccessInformer.Click();
		}
	}
}

﻿namespace Mighty.Call.Ru.Tests.Yandex.Telephony
{
	using OpenQA.Selenium;
	using OpenQA.Selenium.Support.PageObjects;

	/// <summary>
	/// The auth page.
	/// </summary>
	public class AuthPage : BasePage
	{
		/// <summary>
		/// Gets or sets the login button.
		/// </summary>
		[FindsBy(How = How.XPath, Using = ("//*[@id=\"mount\"]/div/div[1]/div/div[2]/div/div[1]/div[1]/div[3]/button"))]
		private IWebElement LoginButton { get; set; }

		/// <summary>
		/// Gets or sets the login.
		/// </summary>
		private IWebElement Login { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		private IWebElement Password { get; set; }

		/// <summary>
		/// The set login.
		/// </summary>
		/// <param name="value">
		/// The value.
		/// </param>
		public void SetLogin(string value)
		{
			Login = Driver.FindElement(By.XPath("//*[@id=\"root\"]/div/div[2]/div[1]/div/div/div/div/div/form/div[1]/label/input"));
			Login.SendKeys(value);			
		}

		/// <summary>
		/// The set password.
		/// </summary>
		/// <param name="value">
		/// The value.
		/// </param>
		public void SetPassword(string value)
		{
			Password = Driver.FindElement(By.XPath("//*[@id=\"root\"]/div/div[2]/div[1]/div/div/div/div/div/form/div[2]/label/input"));
			Password.SendKeys(value);
		}

		/// <summary>
		/// The login button click.
		/// </summary>
		public void LoginButtonClick()
		{
			LoginButton = Driver.FindElement(By.ClassName("passport-Button-Text"));
			LoginButton.Click();
		}

		public AuthPage(IWebDriver driver)
			: base(driver)
		{

		}

		public void LoginAs(string login, string password)
		{
			SetLogin(login);
			SetPassword(password);
			LoginButtonClick();
		}
	}
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainPage.cs" company="MightyCall.ru">
//   Test task
// </copyright>
// <summary>
//   The main page.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests.Yandex.Passport
{
	using OpenQA.Selenium;

	/// <summary>
	/// The main page.
	/// </summary>
	public class MainPage : BasePage
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MainPage"/> class.
		/// </summary>
		/// <param name="driver">
		/// The driver.
		/// </param>
		public MainPage(IWebDriver driver)
			: base(driver)
		{
		}

		/// <summary>
		/// Gets or sets the login button.
		/// </summary>
		private IWebElement LoginButton { get; set; }

		/// <summary>
		/// Gets or sets the connect button.
		/// </summary>
		private IWebElement ConnectButton { get; set; }

		/// <summary>
		/// The connect button click.
		/// </summary>
		public void ConnectButtonClick()
		{
			ConnectButton = Driver.FindElement(By.XPath("//*[@id=\"mount\"]/div/div[1]/div/div[2]/div/div[1]/div[1]/div[3]/button"));
			ConnectButton.Click();
		}

		/// <summary>
		/// The login button click.
		/// </summary>
		public void LoginButtonClick()
		{
			LoginButton = Driver.FindElement(By.XPath("//*[@id=\"mount\"]/div/div[1]/div/div[1]/div[4]/div[2]/a"));
			LoginButton.Click();
		}
	}
}

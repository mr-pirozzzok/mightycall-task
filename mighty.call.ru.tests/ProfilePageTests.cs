﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProfilePageTests.cs" company="MightyCall">
//   Test task
// </copyright>
// <summary>
//   Defines the UnitTest1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Mighty.Call.Ru.Tests
{
	using System;
	using System.Linq;

	using Mighty.Call.Ru.Tests.Helpers;
	using Mighty.Call.Ru.Tests.Yandex.Passport;
	using Mighty.Call.Ru.Tests.Yandex.Telephony;
	using Mighty.Call.Ru.Tests.Yandex.Telephony.Profile;
	using Mighty.Call.Ru.Tests.Yandex.Telephony.Profile.Details;

	using NLog;

	using NUnit.Framework;

	using OpenQA.Selenium;
	using OpenQA.Selenium.Interactions;
	using OpenQA.Selenium.Support.UI;

	using static OpenQA.Selenium.Support.UI.ExpectedConditions;

	/// <summary>
	/// The profile page tests.
	/// </summary>
	[TestFixture]
	public class ProfilePageTests
	{	
		/// <summary>
		/// The helper.
		/// </summary>
		private readonly WebHelper helper = new WebHelper().GetValidUser();

		/// <summary>
		/// Gets or sets the profile page.
		/// </summary>
		private ProfilePage ProfilePage { get; set; }

		/// <summary>
		/// Gets or sets the private settings block.
		/// </summary>
		private PrivateSettingsBlock PrivateSettingsBlock { get; set; }

		/// <summary>
		/// Gets or sets the auth page.
		/// </summary>
		private AuthPage AuthPage { get; set; }

		/// <summary>
		/// Gets or sets the main page.
		/// </summary>
		private MainPage MainPage { get; set; }

		/// <summary>
		/// The logger.
		/// </summary>
		private readonly ILogger logger = LogManager.GetLogger("ProfilePageTests");
		
		/// <summary>
		/// Gets or sets the driver.
		/// </summary>
		private IWebDriver Driver { get; set; }

		#region setup

		/// <summary>
		/// The for each test setup.
		/// </summary>
		[SetUp]
		public void ForEachTestSetup()
		{
			Driver = WebDriversHelper.GetChromeDriver();
			AuthPrefer();
			ProfilePage = new ProfilePage(Driver);
			ProfilePage.AvatarIcoClick();
			ProfilePage.ProfileItemClick();
			Assert.AreEqual(Driver.Url, "https://yandex.mightycall.ru/MightyCall/Profile#/details");
			PrivateSettingsBlock = ProfilePage.PrivateSettingsBlock;
		}

		/// <summary>
		/// The for each test tear down.
		/// </summary>
		[TearDown]
		public void ForEachTestTearDown()
		{
			try
			{
				Driver.Quit();
			}
			catch (Exception e)
			{
				logger.Error(e);
			}
		}

		#endregion setup

		/// <summary>
		/// The change first name test cancel update should be existing value in expected field.
		/// </summary>
		[Test]
		public void ChangeFirstNameTestCancelUpdateShouldBeExistingValueInExpectedField()
		{
			var testName = "abc123";
			var firstName = PrivateSettingsBlock.GetFirstName();
			PrivateSettingsBlock.SetFirstName(testName);
			PrivateSettingsBlock.CancelButtonClick();

			new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(ElementIsVisible(By.Id("FirstName")));
			Assert.AreEqual(firstName, PrivateSettingsBlock.GetFirstName());

		}

		/// <summary>
		/// The change last name test cancel update should be existing value in expected field.
		/// </summary>
		[Test]
		public void ChangeLastNameTestCancelUpdateShouldBeExistingValueInExpectedField()
		{
			var testValue = "abc123456";
			var lastName = PrivateSettingsBlock.GetLastName();
			PrivateSettingsBlock.SetLastName(testValue);
			PrivateSettingsBlock.CancelButtonClick();

			new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(ElementIsVisible(By.Id("LastName")));
			Assert.AreEqual(lastName, PrivateSettingsBlock.GetLastName());
		}

		/// <summary>
		/// The change first name test save update should be existing value in expected field.
		/// </summary>
		[Test]
		public void ChangeFirstNameTestSaveUpdateShouldBeExistingValueInExpectedField()
		{
			var testName = "abc123";
			PrivateSettingsBlock.ClearFirstName();
			PrivateSettingsBlock.SetFirstName(testName);
			PrivateSettingsBlock.SaveChangesButtonClick();
			PrivateSettingsBlock.CloseSuccessUiInformer();
			Assert.IsFalse(PrivateSettingsBlock.SuccessInformer.Displayed);
			Assert.AreEqual(testName, PrivateSettingsBlock.GetFirstName());
		}

		/// <summary>
		/// The change of first name to string bigger then max input string test save update should display expected element.
		/// </summary>
		[Test]
		public void ChangeFirstNameToStringBiggerThenMaxInputStringTestSaveUpdateShouldDisplayExpectedElement()
		{
			var testName = helper.GetRandomString(helper.MaxInputDataLength + 1);
			Assert.IsTrue(testName.Length > helper.MaxInputDataLength);
			PrivateSettingsBlock.ClearFirstName();
			PrivateSettingsBlock.SetFirstName(testName);
			PrivateSettingsBlock.SaveChangesButtonClick();

			var firstNameCaption = PrivateSettingsBlock.FirstNameCaption;
			var firstNameCaptionColor = firstNameCaption.GetCssValue("color");
			var expectedValue = "rgba(169, 68, 66, 1)";
			Assert.AreEqual(firstNameCaptionColor, expectedValue);
			new Actions(Driver).MoveToElement(PrivateSettingsBlock.FirstName).Click().Build().Perform();
			var validationInformElement = PrivateSettingsBlock.InvalidDataMessage;
			Assert.IsTrue(validationInformElement.Displayed);
		}

		/// <summary>
		/// The change first name to empty string test save update should display expected element.
		/// </summary>
		[Test]
		public void ChangeFirstNameToEmptyStringTestSaveUpdateShouldDisplayExpectedElement()
		{
			PrivateSettingsBlock.ClearFirstName();
			PrivateSettingsBlock.SaveChangesButtonClick();

			var firstNameCaption = PrivateSettingsBlock.FirstNameCaption;
			var firstNameCaptionColor = firstNameCaption.GetCssValue("color");
			var expectedValue = "rgba(169, 68, 66, 1)";
			Assert.AreEqual(firstNameCaptionColor, expectedValue);
			new Actions(Driver).MoveToElement(PrivateSettingsBlock.FirstName).Click().Build().Perform();
			var validationInformElement = PrivateSettingsBlock.InvalidDataMessage;
			Assert.IsTrue(validationInformElement.Displayed);
		}

		/// <summary>
		/// The change last name test save update should be existing value in expected field.
		/// </summary>
		[Test]
		public void ChangeLastNameTestSaveUpdateShouldBeExistingValueInExpectedField()
		{
			var testName = "abc123456";
			PrivateSettingsBlock.ClearLastName();
			PrivateSettingsBlock.SetLastName(testName);
			PrivateSettingsBlock.SaveChangesButtonClick();
			PrivateSettingsBlock.CloseSuccessUiInformer();

			Assert.IsFalse(PrivateSettingsBlock.SuccessInformer.Displayed);
			Assert.AreEqual(testName, PrivateSettingsBlock.GetLastName());
		}

		/// <summary>
		/// The change last name to string bigger then max input string test save update should display expected element.
		/// </summary>
		[Test]
		public void ChangeLastNameToStringBiggerThenMaxInputStringTestSaveUpdateShouldDisplayExpectedElement()
		{
			
			var testInput = helper.GetRandomString(helper.MaxInputDataLength + 1);
			Assert.IsTrue(testInput.Length > helper.MaxInputDataLength);
			PrivateSettingsBlock.ClearLastName();
			PrivateSettingsBlock.SetFirstName(testInput);
			PrivateSettingsBlock.SaveChangesButtonClick();

			var lastNameCaption = PrivateSettingsBlock.LastNameCaption;
			var lastNameCaptionColor = lastNameCaption.GetCssValue("color");
			var expectedValue = "rgba(169, 68, 66, 1)";
			Assert.AreEqual(lastNameCaptionColor, expectedValue);
			new Actions(Driver).MoveToElement(PrivateSettingsBlock.LastName).Click().Build().Perform();
			var validationInformElement = PrivateSettingsBlock.InvalidDataMessage;
			Assert.IsTrue(validationInformElement.Displayed);
		}

		/// <summary>
		/// The change last name to empty string test save update should display expected element.
		/// </summary>
		[Test]
		public void ChangeLastNameToEmptyStringSaveUpdateShouldDisplayExpectedElement()
		{
			
			PrivateSettingsBlock.ClearLastName();
			PrivateSettingsBlock.SaveChangesButtonClick();
			var lastNameCaption = PrivateSettingsBlock.LastNameCaption;
			var lastNameCaptionColor = lastNameCaption.GetCssValue("color");
			var expectedValue = "rgba(169, 68, 66, 1)";
			Assert.AreEqual(lastNameCaptionColor, expectedValue);
			new Actions(Driver).MoveToElement(PrivateSettingsBlock.LastName).Click().Build().Perform();
			var validationInformElement = PrivateSettingsBlock.InvalidDataMessage;
			Assert.IsTrue(validationInformElement.Displayed);
		}

		/// <summary>
		/// The change time zone test save update should be existing value in expected field.
		/// </summary>
		[Test]
		public void ChangeTimeZoneTestSaveUpdateShouldBeExistingValueInExpectedField()
		{
			new WebDriverWait(Driver, new TimeSpan(0, 0, 0, 10)).Until(
				ExpectedConditions.VisibilityOfAllElementsLocatedBy(
					By.CssSelector(
						"#ProfileViewModel_Forms_profileDetailsForm > div.mc-details-height.mc-space-below-top-menu.mc-details-padding.ng-scope > div > div.col-lg-6 > div > div:nth-child(4) > div > div")));
			var oldTimeZone = PrivateSettingsBlock.TimeZone.Text;
			var timeZoneListElem = PrivateSettingsBlock.TimeZone;
			
			new Actions(Driver).MoveToElement(timeZoneListElem).Click().Build().Perform();

			var selectDrop = Driver.FindElement(By.Id("select2-drop"));
			var timeZoneLst = selectDrop.Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
			Assert.IsNotEmpty(timeZoneLst);
			var rnd = new Random();
			int randomIdx = rnd.Next(timeZoneLst.Count);
			var newSelectedTimeZone = timeZoneLst.ElementAt(randomIdx);
			var industries = Driver.FindElement(By.Id("select2-results-1"));
			var links = industries.FindElements(By.TagName("li"));

			new Actions(Driver).MoveToElement(links.FirstOrDefault(li => li.Text.Equals(newSelectedTimeZone))).Click().Build()
				.Perform();

			PrivateSettingsBlock.SaveChangesButtonClick();
			PrivateSettingsBlock.CloseSuccessUiInformer();
			var newTimeZone = PrivateSettingsBlock.TimeZone.Text;
			Assert.AreEqual(newSelectedTimeZone, newTimeZone);
			Assert.AreNotEqual(oldTimeZone, newSelectedTimeZone);
		}

		/// <summary>
		/// The change time zone test cancel update should be existing value in expected field.
		/// </summary>
		[Test]
		public void ChangeTimeZoneTestCancelUpdateShouldBeExistingValueInExpectedField()
		{
			new WebDriverWait(Driver, new TimeSpan(0, 0, 0, 10)).Until(
				ExpectedConditions.VisibilityOfAllElementsLocatedBy(
					By.CssSelector(
						"#ProfileViewModel_Forms_profileDetailsForm > div.mc-details-height.mc-space-below-top-menu.mc-details-padding.ng-scope > div > div.col-lg-6 > div > div:nth-child(4) > div > div")));
			var oldTimeZone = PrivateSettingsBlock.TimeZone.Text;
			var timeZoneListElem = PrivateSettingsBlock.TimeZone;
			new Actions(Driver).MoveToElement(timeZoneListElem).Click().Build().Perform();

			var selectDrop = Driver.FindElement(By.Id("select2-drop"));
			var timeZoneLst = selectDrop.Text.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
			Assert.IsNotEmpty(timeZoneLst);
			var rnd = new Random();
			int randomIdx = rnd.Next(timeZoneLst.Count);
			var newSelectedTimeZone = timeZoneLst.ElementAt(randomIdx);
			var industries = Driver.FindElement(By.Id("select2-results-1"));
			var links = industries.FindElements(By.TagName("li"));

			new Actions(Driver).MoveToElement(links.FirstOrDefault(li => li.Text.Equals(newSelectedTimeZone))).Click().Build()
				.Perform();

			PrivateSettingsBlock.CancelButtonClick();
			new WebDriverWait(Driver, new TimeSpan(0, 0, 0, 10)).Until(
				ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.CssSelector("span[id^=\'select2-chosen-\']")));
			var newTimeZone = PrivateSettingsBlock.TimeZone.Text;
			Assert.AreEqual(oldTimeZone, newTimeZone);
		}

		/// <summary>
		/// The auth prefer.
		/// </summary>
		private void AuthPrefer()
		{
			Driver.Navigate().GoToUrl(helper.Url);
			MainPage = new MainPage(Driver);
			MainPage.ConnectButtonClick();
			AuthPage = new AuthPage(Driver);
			AuthPage.LoginAs(helper.Login, helper.Password);
		}
	}
}
